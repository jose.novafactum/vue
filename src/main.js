import { createApp } from 'vue'
import App from './App.vue'
import router from './router/router'

import VueSweetalert2 from 'vue-sweetalert2'
import 'sweetalert2/dist/sweetalert2.min.css'

import BootstrapVue3 from 'bootstrap-vue-3'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue-3/dist/bootstrap-vue-3.css'

import estilos from './assets/css/estilos.css'

const myapp = createApp(App)
myapp.use(BootstrapVue3)
myapp.use(estilos)
myapp.use(router)
myapp.use(VueSweetalert2)
myapp.mount('#app')