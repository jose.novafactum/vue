import { createWebHistory, createRouter } from "vue-router";

import Lobby from '../views/Lobby.vue'
import Home from '../views/Home.vue'
import Expo from '../views/Expo.vue'
import Stands from '../views/Stands.vue'
import FromRegistro from '../views/FromRegistro.vue'
import Login from '../views/Login.vue'

import Contacto from '../views/Contacto.vue'
import Param from '../views/Param.vue'
import Project from '../views/Project.vue';

const routes = [{
        path: "/",
        name: "Login",
        component: Login
    },
    {
        path: "/registro",
        name: 'Registro',
        component: FromRegistro
    },
    {
        path: '/home',
        name: 'Home',
        component: Home
    },
    {
        path: '/lobby',
        name: 'Lobby',
        component: Lobby
    },
    {
        path: '/expocomercial',
        name: "Expo",
        component: Expo
    },
    {
        path: "/Stands/:nombre/:categoria/:idevento",
        name: "Stands",
        component: Stands,
    },
    {
        path: '/contacto',
        name: 'Contacto',
        component: Contacto
    },
    {
        path: '/param/:nombre',
        name: 'Param',
        component: Param,
    },
    {
        path: "/proyecto",
        name: "Project",
        component: Project,
    }

]

const router = createRouter({
    history: createWebHistory(),
    routes,
});


export default router